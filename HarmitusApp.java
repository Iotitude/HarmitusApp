import java.io.IOException;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.kaaproject.kaa.client.DesktopKaaPlatformContext;
import org.kaaproject.kaa.client.Kaa;
import org.kaaproject.kaa.client.KaaClient;
import org.kaaproject.kaa.client.SimpleKaaClientStateListener;
import org.kaaproject.kaa.client.configuration.base.ConfigurationListener;
import org.kaaproject.kaa.client.configuration.base.SimpleConfigurationStorage;
import org.kaaproject.kaa.client.logging.strategies.RecordCountLogUploadStrategy;

import iotitude.kaaplat.endpoint.config.HarmitusConfig01;
import iotitude.kaaplat.endpoint.log.HarmitusLog;

public class HarmitusApp {

    private static final Logger LOG = LoggerFactory.getLogger(HarmitusApp.class);

    private static KaaClient kaaClient;

    private static ScheduledFuture<?> scheduledFuture;
    private static ScheduledExecutorService scheduledExecutorService;

    public static void main(String args[]) {
      LOG.info("HELLO");

      scheduledExecutorService = Executors.newScheduledThreadPool(1);

      DesktopKaaPlatformContext desktopKaaPlatformContext = new DesktopKaaPlatformContext();
      kaaClient = Kaa.newClient(desktopKaaPlatformContext, new FirstKaaClientStateListener(), true);

      RecordCountLogUploadStrategy strategy = new RecordCountLogUploadStrategy(1);
      strategy.setMaxParallelUploads(1);
      kaaClient.setLogUploadStrategy(strategy);

      kaaClient.setConfigurationStorage(new SimpleConfigurationStorage(desktopKaaPlatformContext, "saved_config.cfg"));

      kaaClient.addConfigurationListener(new ConfigurationListener() {
          @Override
          public void onConfigurationUpdate(HarmitusConfig01 configuration) {
              LOG.info("Received configuration data. New sample period: {}", configuration.getInterval());
              // onChangedConfiguration(TimeUnit.SECONDS.toMillis(configuration.getSamplePeriod()));
          }
      });

      kaaClient.start();
      LOG.info("--= Press any key to exit =--");
      try {
          System.in.read();
      } catch (IOException e) {
          LOG.error("IOException has occurred: {}", e.getMessage());
      }
      LOG.info("Stopping...");
      scheduledExecutorService.shutdown();
      kaaClient.stop();
    }

    private static long getUnixTimestamp() {
      return System.currentTimeMillis() / 1000L;
    }

    private static int getHarmitusValue() {
      return new Random().nextInt(4);
    }

    private static void onKaaStarted(long time) {
        if (time <= 0) {
            LOG.error("Wrong time is used. Please, check your configuration!");
            kaaClient.stop();
            System.exit(0);
        }

        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(
                new Runnable() {
                    @Override
                    public void run() {
                        int value = getHarmitusValue();
                        kaaClient.addLogRecord(new HarmitusLog(value, getUnixTimestamp()));
                        LOG.info("Harmitus level: {}", value);
                    }
                }, 0, time, TimeUnit.MILLISECONDS);
    }

    private static class FirstKaaClientStateListener extends SimpleKaaClientStateListener {

        @Override
        public void onStarted() {
            super.onStarted();
            LOG.info("Kaa client started");
            HarmitusConfig01 configuration = kaaClient.getConfiguration();
            LOG.info("Default interval: {}", configuration.getInterval());
            onKaaStarted(TimeUnit.SECONDS.toMillis(configuration.getInterval()));
        }

        @Override
        public void onStopped() {
            super.onStopped();
            LOG.info("Kaa client stopped");
        }
    }
}