# Use the image for Openjdk 8 and Alpine 3.6
FROM openjdk:8-jdk-alpine
# Copy required files to /usr
COPY start.sh /usr/
COPY *.jar /usr/
COPY compile.sh /usr/
COPY HarmitusApp.java /usr/
COPY Manifest.txt /usr/
# Make the script runnable
RUN chmod 777 /usr/compile.sh
# Expose ports and run the script
EXPOSE 8080 8082 25 20 9888 9889 9997 9999 22 2181 9090 9094 9093 9998
ENTRYPOINT ["/bin/sh", "/usr/start.sh"]