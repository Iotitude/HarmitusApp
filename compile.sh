#! /bin/sh

usage="
usage: compile [options]
  options:
    -j create a jar file that is ready to be distributed.
"

JAR_NAME="HarmitusApp.jar"

compile() {
    cd /usr/
    echo "Compiling"
    mkdir -p build

    if [ "$(javac -d build -cp harmitus-kaa-sdk.jar:pi4j-core.jar HarmitusApp.java)" = 0 ]; then
        echo "Compilation failed"
    else
        echo "Compiled successfully"
    fi
}

create_jar() {
    echo ""
    echo "Creating a jar"
    mkdir -p dist
    cd build
    jar cfm $JAR_NAME ../Manifest.txt ./*.class
    mv $JAR_NAME ../dist/
    cd ..
    cp ./*.jar dist/
    echo "Created $JAR_NAME in dist/"
}

compile

while getopts ":j" opt; do
    case $opt in
        j)
            create_jar
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            echo "$usage"
            exit 1
            ;;
    esac
done

exit
