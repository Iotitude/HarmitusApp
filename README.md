
## Compiling

Just run compile.sh

`./compile.sh`

To create a jar run

`./compile.sh -j`


## Transferring to Raspberry Pi

`rsync -av . pi@192.168.51.216:/home/pi`


## Running

ssh to your Raspberry Pi and run

`java -cp pi4j-core.jar:harmitus-kaa-sdk.jar:slf4j-simple-1.7.21.jar:. HarmitusApp`

