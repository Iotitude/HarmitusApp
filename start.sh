#! /bin/sh

# ====================================================================================================
# dependancies

apk update
apk add jq
apk add curl

until $(curl --output /dev/null --silent --head --fail kaa:8080); do
    sleep 5
done

# ====================================================================================================
# admin tunnus

curl -v -S -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' 'http://kaa:8080/kaaAdmin/rest/api/auth/createKaaAdmin?username=kaaAdmin&password=kaaadmin'

# ====================================================================================================
# tenant

curl -v -S -u kaaAdmin:kaaadmin -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
  "id": "1",
  "name": "ktenant"
}' 'http://kaa:8080/kaaAdmin/rest/api/tenant'

# ====================================================================================================
# tenant admin

TENANT_TEMP_PASS=$(curl -v -S -u kaaAdmin:kaaadmin -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
  "username": "kadmin",
  "tenantId": "1",
  "authority": "TENANT_ADMIN",
  "mail": "kadmin@iotitude.fi",
  "tempPassword": ""
}' 'http://kaa:8080/kaaAdmin/rest/api/user?doSendTempPassword=false' | jq -r ".tempPassword")


# ====================================================================================================
# tenant admin password change

curl -v -S -u kaaAdmin:kaaadmin -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' "http://kaa:8080/kaaAdmin/rest/api/auth/changePassword?username=kadmin&oldPassword=$TENANT_TEMP_PASS&newPassword=kadmin"

# ====================================================================================================
# Application

APPLICATION_TOKEN=$(curl -v -S -u kadmin:kadmin -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
  "id": "",
  "applicationToken": "",
  "name": "HarmitusApp",
  "sequenceNumber": 0,
  "tenantId": "1",
  "credentialsServiceName": ""
}' 'http://kaa:8080/kaaAdmin/rest/api/application' | jq -r ".applicationToken")

# ====================================================================================================
# Developer

TENANT_DEV_TEMP_PASS=$(curl -v -S -u kadmin:kadmin -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
  "username": "kdevel",
  "tenantId": "1",
  "authority": "TENANT_DEVELOPER",
  "mail": "kdevel@iotitude.fi",
  "tempPassword": ""
}' 'http://kaa:8080/kaaAdmin/rest/api/user?doSendTempPassword=false' | jq -r ".tempPassword")

# ====================================================================================================
# Developer password change

curl -v -S -u kaaAdmin:kaaadmin -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' "http://kaa:8080/kaaAdmin/rest/api/auth/changePassword?username=kdevel&oldPassword=$TENANT_DEV_TEMP_PASS&newPassword=kdevel"

# ====================================================================================================
# Schemes

curl -v -S -u kdevel:kdevel -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' "http://kaa:8080/kaaAdmin/rest/api/CTL/saveSchema?body=%7B%20%22type%22%20%3A%20%22record%22%2C%22name%22%20%3A%20%22HarmitusLog%22%2C%20%22namespace%22%20%3A%20%22iotitude.kaaplat.endpoint.log%22%2C%20%22fields%22%20%3A%20%5B%20%7B%20%22name%22%20%3A%20%22harmitusLevel%22%2C%20%22type%22%20%3A%20%22int%22%20%7D%2C%20%7B%20%22name%22%20%3A%20%22timestamp%22%2C%20%22type%22%20%3A%20%22long%22%20%7D%20%5D%2C%20%22version%22%20%3A%201%2C%20%22dependencies%22%20%3A%20%5B%20%5D%2C%20%22displayName%22%20%3A%20%22HarmitusLog%22%20%7D&tenantId=1&applicationToken=$APPLICATION_TOKEN"

curl -v -S -u kdevel:kdevel -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' "http://kaa:8080/kaaAdmin/rest/api/CTL/saveSchema?body=%7B%20%20%20%22type%22%20%3A%20%22record%22%2C%20%20%20%22name%22%20%3A%20%22HarmitusConfig01%22%2C%20%20%20%22namespace%22%20%3A%20%22iotitude.kaaplat.endpoint.config%22%2C%20%20%20%22fields%22%20%3A%20%5B%20%7B%20%20%20%20%20%22name%22%20%3A%20%22interval%22%2C%20%20%20%20%20%22type%22%20%3A%20%5B%20%22int%22%2C%20%22null%22%20%5D%2C%20%20%20%20%20%22displayName%22%20%3A%20%22interval%22%2C%20%20%20%20%20%22displayPrompt%22%20%3A%20%22%22%2C%20%20%20%20%20%22by_default%22%20%3A%201%20%20%20%7D%20%5D%2C%20%20%20%22version%22%20%3A%201%2C%20%20%20%22dependencies%22%20%3A%20%5B%20%5D%2C%20%20%20%22displayName%22%20%3A%20%22HarmitusConfig%22%20%7D&tenantId=1&applicationToken=$APPLICATION_TOKEN"

# ====================================================================================================
# Configuration schema

curl -v -S -u kdevel:kdevel -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
  "id": "",
  "version": 1,
  "applicationId": "1",
  "createdTime": 0,
  "createdUsername": "",
  "name": "HarmitusConfig",
  "description": "",
  "ctlSchemaId": "3",
  "protocolSchema": "",
  "baseSchema": "{\"type\":\"record\",\"name\":\"HarmitusConfig01\",\"namespace\":\"iotitude.kaaplat.endpoint.config\",\"fields\":[{\"name\":\"interval\",\"type\":[\"int\",\"null\"],\"displayName\":\"interval\",\"displayPrompt\":\"\",\"by_default\":1},{\"name\":\"__uuid\",\"type\":[{\"type\":\"fixed\",\"name\":\"uuidT\",\"namespace\":\"org.kaaproject.configuration\",\"size\":16},\"null\"],\"displayName\":\"Record Id\",\"fieldAccess\":\"read_only\"}],\"version\":1,\"displayName\":\"HarmitusConfig\"}",
  "overrideSchema": "{\"type\":\"record\",\"name\":\"HarmitusConfig01\",\"namespace\":\"iotitude.kaaplat.endpoint.config\",\"fields\":[{\"name\":\"interval\",\"type\":[\"int\",\"null\",{\"type\":\"enum\",\"name\":\"unchangedT\",\"namespace\":\"org.kaaproject.configuration\",\"symbols\":[\"unchanged\"]}],\"displayName\":\"interval\",\"displayPrompt\":\"\",\"by_default\":1},{\"name\":\"__uuid\",\"type\":[{\"type\":\"fixed\",\"name\":\"uuidT\",\"namespace\":\"org.kaaproject.configuration\",\"size\":16},\"null\"],\"displayName\":\"Record Id\",\"fieldAccess\":\"read_only\"}],\"version\":1,\"displayName\":\"HarmitusConfig\"}",
  "status": "ACTIVE"
}' 'http://kaa:8080/kaaAdmin/rest/api/saveConfigurationSchema'

# ====================================================================================================
# Configuration

curl -v -S -u kdevel:kdevel -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
  "id": "",
  "applicationId": "1",
  "endpointGroupId": "1",
  "sequenceNumber": 0,
  "description": "",
  "createdTime": 0,
  "lastModifyTime": 0,
  "activatedTime": 0,
  "deactivatedTime": 0,
  "createdUsername": "",
  "modifiedUsername": "",
  "activatedUsername": "",
  "deactivatedUsername": "",
  "body": "{\"type\" : \"record\", \"name\" : \"HarmitusConfig01\", \"namespace\" : \"iotitude.kaaplat.endpoint.config\", \"fields\" : [ { \"name\" : \"interval\", \"type\" : [ \"int\", \"null\" ], \"displayName\" : \"interval\", \"displayPrompt\" : \"\", \"by_default\" : 1 } ], \"version\" : 1, \"dependencies\" : [ ], \"displayName\" : \"HarmitusConfig\" }",
  "status": "ACTIVE",
  "endpointCount": 0,
  "version": 1,
  "schemaVersion": 1,
  "schemaId": "6",
  "protocolSchema": ""
}' 'http://kaa:8080/kaaAdmin/rest/api/configuration'

# ====================================================================================================
# Log schema

curl -v -S -u kdevel:kdevel -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
  "id": "",
  "version": 1,
  "applicationId": "1",
  "createdTime": 0,
  "createdUsername": "",
  "name": "HarmitusLog",
  "description": "",
  "ctlSchemaId": "2"
}' 'http://kaa:8080/kaaAdmin/rest/api/saveLogSchema'

# ====================================================================================================
# SDK profile

curl -v -S -u kdevel:kdevel -X POST -H 'Content-Type: application/json' -d'{ "applicationId": "1", "configurationSchemaVersion": 2, "logSchemaVersion": 2, "name": "HarmitusSDK2", "notificationSchemaVersion": 1, "profileSchemaVersion": 0 }' "http://kaa:8080/kaaAdmin/rest/api/createSdkProfile"

sleep 70

# ====================================================================================================
# Log appender

curl -v -S -u kdevel:kdevel -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
  "name": "CassandraAppender",
  "description": "",
  "createdUsername": "",
  "createdTime": 0,
  "id": "",
  "applicationId": "1",
  "pluginTypeName": "Cassandra",
  "pluginClassName": "org.kaaproject.kaa.server.appenders.cassandra.appender.CassandraLogAppender",
  "jsonConfiguration": "{\"cassandraServers\":[{\"host\":\"cassandra\",\"port\":9042}],\"cassandraCredential\":{\"org.kaaproject.kaa.server.appenders.cassandra.config.gen.CassandraCredential\":{\"user\":\"user\",\"password\":\"password\"}},\"keySpace\":\"kaa\",\"tableNamePattern\":\"harmitus_logs\",\"columnMapping\":[{\"type\":\"EVENT_FIELD\",\"value\":{\"string\":\"harmitusLevel\"},\"columnName\":\"harmitus_level\",\"columnType\":\"INT\",\"partitionKey\":false,\"clusteringKey\":false},{\"type\":\"HEADER_FIELD\",\"value\":{\"string\":\"endpointKeyHash\"},\"columnName\":\"endpoint_keyhash\",\"columnType\":\"TEXT\",\"partitionKey\":false,\"clusteringKey\":false},{\"type\":\"TS\",\"value\":{\"string\":\"yyyy-MM-dd\"},\"columnName\":\"date\",\"columnType\":\"TEXT\",\"partitionKey\":false,\"clusteringKey\":true},{\"type\":\"UUID\",\"value\":{\"string\":\"\"},\"columnName\":\"id\",\"columnType\":\"UUID\",\"partitionKey\":true,\"clusteringKey\":false},{\"type\":\"EVENT_FIELD\",\"value\":{\"string\":\"timestamp\"},\"columnName\":\"timestamp\",\"columnType\":\"BIGINT\",\"partitionKey\":false,\"clusteringKey\":false}],\"clusteringMapping\":[],\"cassandraBatchType\":{\"org.kaaproject.kaa.server.appenders.cassandra.config.gen.CassandraBatchType\":\"UNLOGGED\"},\"cassandraSocketOption\":null,\"executorThreadPoolSize\":1,\"callbackThreadPoolSize\":2,\"dataTTL\":0,\"cassandraWriteConsistencyLevel\":{\"org.kaaproject.kaa.server.appenders.cassandra.config.gen.CassandraWriteConsistencyLevel\":\"ONE\"},\"cassandraCompression\":{\"org.kaaproject.kaa.server.appenders.cassandra.config.gen.CassandraCompression\":\"NONE\"},\"cassandraExecuteRequestType\":{\"org.kaaproject.kaa.server.appenders.cassandra.config.gen.CassandraExecuteRequestType\":\"SYNC\"}}",
  "applicationToken": '"$APPLICATION_TOKEN"',
  "tenantId": "1",
  "minLogSchemaVersion": 1,
  "maxLogSchemaVersion": 2147483647,
  "confirmDelivery": true,
  "headerStructure": [
    "KEYHASH",
    "VERSION",
    "TIMESTAMP",
    "TOKEN",
    "LSVERSION"
  ]
}' 'http://kaa:8080/kaaAdmin/rest/api/logAppender'


# ====================================================================================================
# SDK

cd /usr/
curl -v -o /usr/harmitus-kaa-sdk.jar -S -u kdevel:kdevel -X POST "http://kaa:8080/kaaAdmin/rest/api/sdk?sdkProfileId=1&targetPlatform=JAVA"
sh compile.sh -j
chmod +x /usr/dist/HarmitusApp.jar
/usr/bin/java -jar /usr/dist/HarmitusApp.jar
